# README.md 編寫範例
# Release Node
### v2.2.3
- 新增功能：
  - logstash 日誌收集

- 部署注意事項：
  - 新增logstash環境變數：
    - LDS_LOGSTASH_01
    - LDS_LOGSTASH_02 
  - 環境變數格式：
    - http://{ IP or FQDN }:{ PORT }
  - 範例：
    ```
    LDS_LOGSTASH_01=http://172.16.0.1:5050
    LDS_LOGSTASH_02=http://172.16.0.2:5050
    ```
  - 此次改版新增部署事項
    - 程式碼已修正DB連線字串優先使用環境變數，無需替換`lds/www/application/v0.032/conf/conf_base.php`檔案
    
    - DB使用的環境變數
      - DB_MASTER
      - DB_SLAVE
      - DB_PORT
      - DB_USER
      - DB_PWD
    - 連線相關設定
      - LDS_CONNECT_TIMEOUT
      - LDS_TIMEOUT
      - LDS_MAX_QUEUE

### v2.2.2 包含之前

- DB設定檔位置：
  - `lds/www/application/v0.032/conf/conf_base.php`
  - DB使用的環境變數(尚未正式使用)
    - DB_MASTER
    - DB_SLAVE
    - DB_PORT
    - DB_USER
    - DB_PWD
- 部署注意事項
  - 替換`lds/www/application/v0.032/conf/conf_base.php` 內ＤＢ設定

# 服務用途
- 主要提供ＸＸＸＸＸＸ

# 部署方式
- 部署工具
  - docker, docker-compose
- deliver
  ```
  $ git clone -b {tag version} git@gitlab.com:gamesoft-video/services/db-server/lcs_lds.git 
  $ cd lds
  $ docker build -t lcs:{tag version} .
  ```
- 服務啟動
  - docker-compose 工具
  ```
  docker-ccompose up -d
  ```
